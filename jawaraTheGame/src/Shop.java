import java.util.ArrayList;
import java.util.Scanner;

class Shop {
    private int pilih;
    private int pilihanBeli;

    Scanner input = new Scanner(System.in);

    public void menushop(ArrayList<Character> characters, ArrayList<Weapon> weapons, ArrayList<Armor> armors, ArrayList<Skill> skills, Player objPlayer){
        System.out.println("+====== APA YANG KAMU BUTUHKAN? ======+\n");
        System.out.println("| [1] Character");
        System.out.println("| [2] Weapon");
        System.out.println("| [3] Armor");
        System.out.println("| [4] Skill");
        System.out.println("| [5] Kembali");
        System.out.print("Pilihan Menu : ");
        pilih = input.nextInt();
        if(pilih==1){
            System.out.println("+====================================+");
            System.out.println("+=========== BUY CHARACTER ==========+");
            System.out.println("+====================================+");

            int i=1;
            for (Character c:characters){
                if (!c.getEnemy()){
                    System.out.printf(" [%d] %s | Health: %d | damage: %d\n", i, c.getName(), c.getHealth(),c.getDamage());
                    System.out.printf(" Harga = %d coin\n\n",c.getPrice());
                    i++;
                }else {
                    break;
                }

            }
            System.out.print("Enter your choice: ");
            pilihanBeli = input.nextInt();
            if (pilihanBeli>0 && pilihanBeli <= characters.size() && !characters.get(pilihanBeli-1).getEnemy()){
                if (objPlayer.getCoin() >= characters.get(pilihanBeli-1).getPrice()){
                    objPlayer.addCharacter(characters.get(pilihanBeli-1));
                    objPlayer.coinWhenBuy(characters.get(pilihanBeli-1).getPrice());
                    System.out.println("Anda berhasil membeli character "+characters.get(pilihanBeli-1).getName());
                    characters.remove(pilihanBeli-1);
                }else {
                    System.out.println("Uang anda tidak mencukupi!");
                }
            }else System.out.println("Input Invalid!");


        }else if(pilih==2){
            System.out.println("+====================================+");
            System.out.println("+============ BUY WEAPON ============+");
            System.out.println("+====================================+");

            int i=1;
            for (Weapon w:weapons){
                //Pengkondisian weapon kepunyaan musuh, maka tidak ditampikan pada shop
                if (w.getIsEnemy())break;
                System.out.printf("[%d] %s | damage: %d\n",i, w.getName(), w.getDamage());
                System.out.printf("Harga: %d coin\n\n", w.getPrice());
                i++;
            }
            System.out.print("Enter your choice: ");
            pilihanBeli = input.nextInt();

            if (pilihanBeli>0 && pilihanBeli <= weapons.size()){
                if (objPlayer.getCoin() >= weapons.get(pilihanBeli-1).getPrice()){
                    objPlayer.addWeapon(weapons.get(pilihanBeli-1));
                    objPlayer.coinWhenBuy(weapons.get(pilihanBeli-1).getPrice());
                    System.out.println("Anda berhasil membeli weapon "+weapons.get(pilihanBeli-1).getName());
                    weapons.remove(pilihanBeli-1);
                }else {
                    System.out.println("Uang anda tidak mencukupi!");
                }
            }else System.out.println("Input Invalid!");
        }else if(pilih==3){
            System.out.println("+====================================+");
            System.out.println("+============ BUY ARMOR =============+");
            System.out.println("+====================================+");
            int i=1;
            for (Armor a:armors){
                //Pengkondisian Armor kepunyaan musuh, maka tidak ditampikan pada shop
                if (a.getIsEnemy())break;
                System.out.printf("[%d] %s | deffence: %d\n",i, a.getName(), a.getDefencePower());
                System.out.printf("Harga: %d coin\n\n", a.getPrice());
                i++;
            }
            System.out.print("Enter your choice: ");
            pilihanBeli = input.nextInt();

            if (pilihanBeli>0 && pilihanBeli <= armors.size()){
                if (objPlayer.getCoin() >= armors.get(pilihanBeli-1).getPrice()){
                    objPlayer.addArmor(armors.get(pilihanBeli-1));
                    objPlayer.coinWhenBuy(armors.get(pilihanBeli-1).getPrice());
                    System.out.println("Anda berhasil membeli armor "+armors.get(pilihanBeli-1).getName());
                    armors.remove(pilihanBeli-1);
                }else {
                    System.out.println("Uang anda tidak mencukupi!");
                }
            }else System.out.println("Input Invalid!");

        }else if(pilih==4){
            System.out.println("+====================================+");
            System.out.println("+============ BUY SKILL =============+");
            System.out.println("+====================================+");
            int i=1;
            for (Skill s:skills){
                System.out.printf("[%d] %s | Damage: %d\n",i, s.getName(), s.getDamage());
                System.out.printf("Harga: %d coin\n\n", s.getPrice());
                i++;
            }
            System.out.print("Enter your choice: ");
            pilihanBeli = input.nextInt();

            if (pilihanBeli>0 && pilihanBeli <= skills.size()){
                if (objPlayer.getCoin() >= skills.get(pilihanBeli-1).getPrice()){
                    objPlayer.addSkill(skills.get(pilihanBeli-1));
                    objPlayer.coinWhenBuy(skills.get(pilihanBeli-1).getPrice());
                    System.out.println("Anda berhasil membeli skill "+skills.get(pilihanBeli-1).getName());
                    skills.remove(pilihanBeli-1);
                }else {
                    System.out.println("Uang anda tidak mencukupi!");
                }
            }else System.out.println("Input Invalid!");
        }else if(pilih==5){
            System.out.println();
        }else{
            System.out.println("Maaf, pilihan menu tidak ada");
        }
    }
}
