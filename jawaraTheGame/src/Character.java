class Character {
    protected String name;
    protected int price;
    protected int health;
    protected int damage;
    protected boolean isEnemy;
    protected String description;
    protected Weapon weaponDefault;
    protected Armor armorDefault;

    Character(String name,int price,int health,int damage,boolean enemy, String description){
        this.name = name;
        this.price = price;
        this.health = health;
        this.damage = damage;
        this.isEnemy = enemy;
        this.description = description;
    }

    public void setWeaponDefault(Weapon weaponDefault) {
        this.weaponDefault = weaponDefault;
    }
    public void setArmorDefault(Armor armorDefault) {
        this.armorDefault = armorDefault;
    }
    public Armor getArmorDefault() {
        return armorDefault;
    }
    public Weapon getWeaponDefault() {
        return weaponDefault;
    }

    public String getName(){
        return this.name;
    }
    public int getPrice(){
        return this.price;
    }
    public int getHealth(){
        return this.health;
    }
    public int getDamage(){
        return this.damage;
    }
    public boolean getEnemy(){
        return this.isEnemy;
    }
    public String getDescription(){
        return this.description;
    }




}