class Skill extends Item{
    private int damage;

    Skill(String name,int price,int damage,String description){
        super(name,price,description);
        this.damage = damage;
    }

    public int getDamage(){
        return this.damage;
    }
}