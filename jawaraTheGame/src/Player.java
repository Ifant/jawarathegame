import java.util.ArrayList;

public class Player{
    private String username;
    protected int damage;
    protected int health;
    private int coin;
    private int stage;
    private boolean battle = true;
    protected int currentLevel;
    Character character=null;
    Weapon weapon=null;
    Armor armor=null;
    Skill skill=null;

    //Storage yang dimiliki
    ArrayList<Character> characters = new ArrayList<>();
    ArrayList<Weapon> weapons = new ArrayList<>();
    ArrayList<Armor> armors = new ArrayList<>();
    ArrayList<Skill> skills = new ArrayList<>();


    public void equipCharacter(int index){
        this.character = characters.get(index) ;
        this.damage = characters.get(index).getDamage();
        this.health = characters.get(index).getHealth();
    }
    public void equipWeapon(int index){
        this.weapon = weapons.get(index) ;
        this.damage += weapons.get(index).getDamage();
    }
    public void equipArmor(int index){
        this.armor = armors.get(index) ;
        this.health += armors.get(index).getDefencePower();
    }

    public void equipSkill(int index){
        this.skill = skills.get(index) ;
        this.damage += skills.get(index).getDamage();
    }

    public void addCharacter(Character objCharacter){
        this.characters.add(objCharacter);
    }
    public void addWeapon(Weapon objWeapon){
        this.weapons.add(objWeapon);
    }
    public void addArmor(Armor objArmor){
        this.armors.add(objArmor);
    }
    public void addSkill(Skill objSkill){
        this.skills.add(objSkill);
    }
    public void coinWhenBuy(int coinBerkurang){
        this.coin -= coinBerkurang;
    }

    public void setUsername(String name){
        this.username = name;
    }
    public String getUsername(){
        return this.username;
    }
    public int getCoin(){
        return this.coin;
    }
    public int getStage(){
        return this.stage;
    }
    public int getHealth(){
        return this.health;
    }

    public void startBattle(){
        this.battle = true;
    }
    public void stopBattle(){
        this.battle = false;
    }
    public boolean getBattle(){
        return this.battle;
    }

    public void attack(Enemy enemy){
        int damage;
        if (this.weapon != null) {
            damage = this.weapon.getDamage() + this.damage;
        }
        else {
            damage = this.damage;
        }
        enemy.getAttack(damage);

        if (this.health>0 && getBattle()){
            System.out.printf("<=======Aksi dari %s=======>\n",this.username);
            System.out.printf("%s berhasil menyerang %s\n",this.username,enemy.getName());
            System.out.printf("%s medapatkan kerusakan -%d\n\n",enemy.getName(),damage);
        }
        else if (this.health<=0){
            System.out.println("Kamu kalah!");
            setCoin(50);
            System.out.printf("Mendapatkan %d coin\n\n",50);
            stopBattle();
        }
    }

    public void getAttack(int damage){
        this.health -= damage;

    }

    public void skill(Enemy enemy){
        int damage;
        damage = this.skill.getDamage();
        enemy.getAttack(damage);

        if (this.health>0 && getBattle()){
            System.out.printf("<=======Aksi dari %s=======>\n",this.username);
            System.out.printf("%s berhasil menyerang menggunakan skill kepada %s\n\n",this.username,enemy.getName());
            System.out.printf("%s medapatkan kerusakan -%d\n\n",enemy.getName(),damage);

        }
        else if (this.health<=0){
            System.out.println("Kamu kalah!");
            setCoin(50);
            System.out.printf("Mendapatkan %d coin\n\n",50);
            stopBattle();
        }
    }

    public void aturHealth(){
        int health;
        if (this.armor != null){
            health = this.armor.getDefencePower();
        }else health = 0;
        this.health += health;
    }

    public void setCoin(int coin){
        this.coin += coin;
    }
    public void setStage(int stage){

        this.stage += stage;
    }

    public void display(){
        System.out.println("Name\t\t: "+ this.username);
        System.out.printf("Character\t: %s (%s)\n", this.character.getName(), this.character.getDescription());
        System.out.println("Health\t\t: "+ this.health);
        System.out.println("Damage\t\t: "+ this.damage);
        if (this.weapon != null) System.out.println("Weapon\t\t: "+ this.weapon.getName()+" (+"+this.weapon.getDamage()+" damage)");
        else System.out.println("Weapon not found");
        if (this.armor != null) System.out.println("Armor\t\t: "+ this.armor.getName());
        else System.out.println("Armor not found");
        if (this.skill != null) System.out.println("Skill\t\t: "+ this.skill.getName()+" ("+this.skill.getDamage()+" damage)");
        else System.out.println("Skill not found");

        System.out.println("");
    }

    public void displayMyCharacter(){
        int z = 1;
        for (Character character: this.characters){
            System.out.printf("[%d] %s | Health: %d | damage: %d\n",z,character.getName(),character.getHealth(),character.getDamage());
            z++;
        }
    }

    public void displayMyWeapon(){
        int z=1;
        for (Weapon w:this.weapons){
            System.out.printf("[%d] %s | damage: %d\n",z,w.getName(),w.getDamage());
            z++;
        }
    }

    public void displayMyArmor(){
        int z=1;
        for (Armor a:this.armors){
            System.out.printf("[%d] %s | deffence: %d\n",z,a.getName(),a.getDefencePower());
            z++;
        }
    }

    public void displayMySkill(){
        int z=1;
        for (Skill s:this.skills){
            System.out.printf("[%d] %s | damage: %d\n",z,s.getName(),s.getDamage());
            z++;
        }
    }


}