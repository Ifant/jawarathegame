public class Enemy extends Player{

    public void setCharacter(Character character){
        this.character = character;
        this.health = character.health;
        this.damage = character.damage;
        if (character.getArmorDefault() != null){
            this.health += character.getArmorDefault().getDefencePower();
        }
    }
    public String getName(){
        return character.getName();
    }

    public void attack(Player objPlayer){
        int damage;
        if (this.character.getWeaponDefault() != null){
            damage = this.damage + this.character.getWeaponDefault().getDamage();
        }else {
            damage = this.damage;
        }
        if (objPlayer.getHealth() - damage < 0){
            damage = objPlayer.getHealth();
        }
        objPlayer.getAttack(damage);

        if (this.health>0 && objPlayer.getBattle()){
            System.out.println("<====Musuh Melakukan Aksi====>");
            System.out.printf("%s menyerang balik\n", getName());
            System.out.printf("%s medapatkan kerusakan -%d\n\n",objPlayer.getUsername(),damage);

        }
        else if (this.health<=0){
            System.out.println("Selamat kamu menang!");
            objPlayer.setCoin(200);
            System.out.printf("Kamu berhak mendapatkan %d coin\n\n",200);
            if (objPlayer.currentLevel == objPlayer.getStage()){//Kondisi level/stage naik
                objPlayer.setStage(1);
            }
            objPlayer.stopBattle();
        }
        if (objPlayer.getHealth()<=0 && this.health >=0){
            System.out.println("Kamu kalah!");
            objPlayer.setCoin(50);
            System.out.printf("Mendapatkan %d coin\n\n",50);
            objPlayer.stopBattle();
        }
    }

    public void skill(Player objPlayer){
        System.out.printf("<======Aksi dari %s======>\n", this.character.getName());
        System.out.printf("%s berhasil menyerang menggunakan skill kepada %s\n\n",this.character.getName(),objPlayer.getUsername());

    }

    @Override
    public void display(){
        System.out.println("Enemy: ");
        System.out.printf("Character\t: %s (%s)\n", this.character.getName(), this.character.getDescription());
        System.out.println("Health\t\t: "+ this.health);
        System.out.println("Damage\t\t: "+ this.damage);
        if (this.character.getWeaponDefault() != null) {
            String namaWeapon = this.character.getWeaponDefault().getName();
            String descWeapon = this.character.getWeaponDefault().getDescription();
            int damageWeapon = this.character.getWeaponDefault().getDamage();
            System.out.println("Weapon\t\t: "+ namaWeapon+" ("+descWeapon+")"+" (+"+damageWeapon+" damage)");
        }
        else System.out.println("Weapon not found");
        if (this.character.getArmorDefault() != null) System.out.println("Armor\t\t: "+ this.character.getArmorDefault().getName());
        else System.out.println("Armor not found");

        System.out.println("");
    }
}
