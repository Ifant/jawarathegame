class Weapon extends Item{
    private int damage;
    private boolean isEnemy;

    Weapon(String name,int price, int damage, String description,boolean isEnemy){
        super(name,price,description);
        this.damage = damage;
        this.isEnemy = isEnemy;
    }

    public int getDamage(){
        return this.damage;
    }
    public boolean getIsEnemy(){
        return this.isEnemy;
    }
}