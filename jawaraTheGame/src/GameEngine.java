import java.util.ArrayList;
import java.util.Scanner;

public class GameEngine {

    public static void main(String[] args) {
        int pilihan;
        int pilCharacter=0;
        int pilWeapon=0;
        int pilArmor=0;
        int pilSkil=0;

        boolean menu = true;
        Scanner input = new Scanner(System.in);

        //INISIALISASI DATA
        Level[] levels = new Level[10];
        ArrayList<Character> characters = new ArrayList<>();
        ArrayList<Weapon> weapons = new ArrayList<>();
        ArrayList<Armor> armors = new ArrayList<>();
        ArrayList<Skill> skills = new ArrayList<>();
        Enemy objEnemy = new Enemy();

        //INPUT DATA-DATA YANG DIPERLUKAN
        levels = inputLevel();//INPUT DATA LEVEL
        weapons = inputWeapon();//INPUT DATA WEAPON
        armors = inputArmor();//INPUT DATA ARMOR
        characters = inputCharacter(weapons,armors);//INPUT DATA CHARACTER
        skills = inputSkill();//INPUT DATA SKILL

        System.out.println("+==================WELCOME TO JAWARA THE GAME==================+");
        about();
        System.out.print("Enter Your Username: ");
        String username = input.nextLine();
        Player player = new Player();
        player.setUsername(username);
        player.setCoin(999999);//set coin awal
        player.setStage(1);
        player.addCharacter(characters.get(0));//Character default
        characters.remove(0);

        //Testing data character enemy
        //testingDataEnemyChar(characters);
        //Menghitung banyaknya total character
        int banyaknyaCharacterAwal;
        banyaknyaCharacterAwal = characters.size();

        while (menu){
            menu(player);
            System.out.print("Enter your choice: ");
            pilihan = input.nextInt();
            //MENU BATTLE
            if (pilihan==1){
                System.out.println("Choose Level: ");
                for (int i=0;i<levels.length;i++){
                    System.out.printf("[%d] Level %d, map: %s ",i+1,levels[i].getStage(),levels[i].getLocation());

                    if (player.getStage()<levels[i].getStage()) System.out.print("(LOCKED)\n");
                    else System.out.println("");
                }
                int pilihanLevel;

                System.out.print("Enter your choice: ");
                pilihanLevel = input.nextInt();

                if (pilihanLevel>player.getStage() || player.character==null){
                    if (pilihanLevel>player.getStage()){
                        System.out.println("Level Locked!");
                    }else {
                        System.out.println("Please choose character first!");
                    }
                }
                //BATTLE DIMULAI
                else {
                    player.startBattle();
                    System.out.printf("+=== SELAMAT DATANG DI LEVEL %d ===+\n",pilihanLevel);
                    System.out.println(levels[pilihanLevel-1].getDescription());
                    System.out.println("==================================");
                    System.out.println("PERTEMPURAN DIMULAI!");

                    player.currentLevel = pilihanLevel;
                    player.equipCharacter(pilCharacter-1);
                    player.aturHealth();

                    //Cek apakah character berkurang
                    int recentCharacterJumlah = 0;
                    if (characters.size() < banyaknyaCharacterAwal){
                        recentCharacterJumlah = characters.size() - banyaknyaCharacterAwal;
                    }
                    objEnemy.setCharacter(characters.get(pilihanLevel+4+recentCharacterJumlah));

                    while (player.getBattle()){
                        player.display();
                        objEnemy.display();

                        System.out.println("Pilihan menyerang: ");
                        System.out.println("[1] Attack");
                        System.out.println("[2] Skill");
                        System.out.print("Masukan pilihan anda: ");
                        int pilMenyerang = input.nextInt();
                        if (pilMenyerang==1){
                            player.attack(objEnemy);
                            objEnemy.attack(player);
                        }else if (pilMenyerang==2){
                            if (player.skill != null){
                                player.skill(objEnemy);
                                objEnemy.attack(player);
                            }else {
                                System.out.println("\n======Kamu Belum Menggunakan Item Skill======");
                            }

                        }else {
                            System.out.println("Input Invalid!");
                        }
                        System.out.println("===================================");
                    }


                }

            }else if(pilihan==2){
                System.out.println("======CHOOSE CHARACTER======");
                player.displayMyCharacter();
                System.out.print("Enter your choice: ");
                pilCharacter= input.nextInt();
                if (pilCharacter>player.characters.size()){
                    System.out.println("Input Invalid!");
                }else {
                    player.equipCharacter(pilCharacter-1);
                    System.out.println("Berhasil memilih character " + player.characters.get(pilCharacter-1).getName());
                }


            }else if(pilihan==3){
                System.out.println("======CHOOSE WEAPON======");
                int banyakWeaponPlayer=0;
                //Pengkondisian untuk menghitung banyaknya weapon yang dapat dimiliki player
                for (Weapon weapon:weapons){
                    if (!weapon.getIsEnemy()) banyakWeaponPlayer++;
                }
                player.displayMyWeapon();
                System.out.printf("Enter your choice: ");
                pilWeapon = input.nextInt();
                if (pilWeapon>banyakWeaponPlayer || player.weapons.size()==0){
                    System.out.println("Input Invalid!");
                }else {
                    player.equipWeapon(pilWeapon-1);
                    System.out.println("Berhasil memilih weapon " + player.weapons.get(pilWeapon-1).getName());
                }

            }else if(pilihan==4){
                System.out.println("======CHOOSE ARMOR======");
                player.displayMyArmor();
                int banyakArmorPlayer=0;
                //Pengkondisian untuk menghitung banyaknya armor yang dapat dimiliki player
                for (Armor armor:armors){
                    if (!armor.getIsEnemy()) banyakArmorPlayer++;
                }
                System.out.printf("Enter your choice: ");
                pilArmor = input.nextInt();
                if (pilArmor>banyakArmorPlayer || player.armors.size()==0){
                    System.out.println("Input Invalid!");
                }else {
                    player.equipArmor(pilArmor-1);
                    System.out.println("Berhasil memilih armor " + player.armors.get(pilArmor-1).getName());
                }

            }else if (pilihan==5){
                System.out.println("======CHOOSE SKILL======");
                player.displayMySkill();
                System.out.printf("Enter your choice: ");
                pilSkil = input.nextInt();
                if (pilSkil>player.skills.size()){
                    System.out.println("Input Invalid!");
                }else {
                    player.equipSkill(pilSkil-1);
                    System.out.println("Berhasil memilih skill " + player.skills.get(pilSkil-1).getName());
                }

            }else if (pilihan==6){
                Shop beli = new Shop();
                beli.menushop(characters,weapons,armors,skills,player);
            }else if (pilihan==7){
                about();
            }
            else if(pilihan==0){
                System.out.println("Terima Kasih");
                break;
            }
        }
    }

    static void testingDataEnemyChar(ArrayList<Character> characters){
        for (Character character:characters){
            if (character.getEnemy() && character.weaponDefault != null || character.armorDefault != null){
                System.out.println("\nNama: " + character.getName());
                System.out.println("Weapon: " + character.getWeaponDefault().getName());
                if (character.armorDefault != null){
                    System.out.println("Armor: " + character.getArmorDefault().getName());
                }
            }
        }
    }

    static void menu(Player objPlayer) {
        System.out.println("+=========JAWARA THE GAME=========+");
        System.out.printf("| Username\t: %s\n",objPlayer.getUsername());
        System.out.printf("| Coin\t\t: %s\n",objPlayer.getCoin());
        System.out.println("| Stage\t\t: "+ objPlayer.getStage());
        System.out.println("| [1] START                       |");
        System.out.println("| [2] CHOOSE CHARACTER            |");
        System.out.println("| [3] CHOOSE WEAPON               |");
        System.out.println("| [4] CHOOSE ARMOR                |");
        System.out.println("| [5] CHOOSE SKILL                |");
        System.out.println("| [6] SHOP                        |");
        System.out.println("| [7] ABOUT                       |");
        System.out.println("| [0] EXIT                        |");

    }

    static Level[] inputLevel(){
        Level[] levels = new Level[10];

        levels[0] = new Level(1,"Cipocok","Pada Level ini kamu akan melawan tokoh Belanda yang bernama Wilhelmina, Wilhelmina merupakan seorang ratu\nyang memimpin Belanda selama lebih dari 50 tahun");
        levels[1] = new Level(2,"Royal","Pada Level ini kamu akan melawan tokoh Belanda yang bernama JB Van heutsz, JB Van heutsz merupakan seorang penguasa\npemerintahan Hindia Belanda");
        levels[2] = new Level(3,"Ciracas","Pada Level ini kamu akan melawan tokoh Belanda yang bernama Godert Van der Capellen, Godert Van der Capellen merupakan seorang penguasa\npemerintahan Hindia Belanda pertama");
        levels[3] = new Level(4,"Taktakan","Pada Level ini kamu akan melawan tokoh Belanda yang bernama Laurens Reael, Laurens Reael merupakan seorang Gubernur-Jenderal\nHindia Belanda yang ketiga");
        levels[4] = new Level(5,"Padarincang","\"Pada Level ini kamu akan melawan tokoh Belanda yang bernama Jenderal Pieter Both, Jenderal Pieter Both merupakan penguasa tertinggi\nuntuk menciptakan monopoli perdagangan antar pulau-pulau di Hindia Belanda yang memulai pos perdagangan di Banten\"");
        levels[5] = new Level(6,"Baros","Pada Level ini kamu akan melawan tokoh Belanda yang bernama Cornelis de houtman, Cornelis de houtman merupakan pemimpin pelayaran pada\npencarian daerah asal rempah-rempah");
        levels[6] = new Level(7,"Pandeglang","Pada Level ini kamu akan melawan tokoh Belanda yang bernama Pieter de Kaizer sama seperti Cornelis de houtman, Pieter de Kaizer juga\nmerupakan pemimpin pelayaran pada pencarian daerah asal rempah-rempah");
        levels[7] = new Level(8,"Tanjung Lesung","Pada Level ini kamu akan melawan tokoh Belanda yang bernama Jenderal Gerard Reynst, Jenderal Gerard Reynst merupakan pemegang saham VOC\n(de Heren XVII) dan diangkat menjadi gubernur Jenderal Hindia Belanda serta diangkat menjadi armada kapal");
        levels[8] = new Level(9,"Kaujon","Pada Level ini kamu akan melawan tokoh Belanda yang bernama Jenderal Jan Pieterszoon coen, Jenderal Jan Pieterszoon coen merupakan Jenderal VOC\nkeempat yang mempunyai ciri khas dari kepemimpinannya adalah politik atau adu domba atau devide et impera");
        levels[9] = new Level(10,"Ciomas","Pada Level ini kamu akan melawan tokoh Belanda yang bernama Jenderal Cornelis van der Lijn, Jenderal Cornelis van der Lijn merupakan seorang\nGubernur yang berhasil memperkuat VOC");
        return levels;
    }

    static ArrayList<Character> inputCharacter(ArrayList<Weapon> weapons, ArrayList<Armor> armors){
        ArrayList<Character> characterArrayList = new ArrayList<>();
        Character[] characters = new Character[16];

        //INPUT JAWARA CHARACTER
        characters[0] = new Character("Aris",0,100,8,false,"JARO");
        characters[1] = new Character("Wawan",1000,100,10,false,"Guru Silat");
        characters[2] = new Character("Magi",2500,110,15,false,"Guru Ilmu Batin");
        characters[3] = new Character("Sanusi",4000,120,20,false,"Pemain Debus");
        characters[4] = new Character("Jendral Listo",7000,125,35,false,"Tentara Wakaf");
        characters[5] = new Character("Abuya Dimayati",11000,140,50,false,"Khodim Kiayi");

        //INPUT COLONIALS CHARACTER
        characters[6] = new Character("Wilhelmina",0,100,5,true,"seorang ratu yang memimpin Belanda selama lebih dari 50 tahun.");
        characters[7] = new Character("JB Van heutsz",0,105,8,true,"seorang penguasa pemerintahan Hindia Belanda");
        characters[8] = new Character("Godert Van der Capellen",0,110,10,true,"seorang penguasa pemerintahan Hindia Belanda pertama");
        characters[9] = new Character("Laurens Reael",0,113,15,true,"seorang Gubernur-Jenderal Hindia Belanda yang ketiga");
        characters[10] = new Character("Jenderal Pieter Both",0,115,20,true,"penguasa tertinggi untuk menciptakan monopoli perdagangan antar pulau-pulau di Hindia Belanda yang memulai pos perdagangan di Banten");
        characters[11] = new Character("Cornelis de houtman",0,120,23,true,"pemimpin pelayaran pada pencarian daerah asal rempah-rempah");
        characters[12] = new Character("Pieter de kaizer ",0,124,25,true,"pemimpin pelayaran pada pencarian daerah asal rempah-rempah");
        characters[13] = new Character("Jenderal Gerard Reynst",0,130,28,true,"pemegang saham VOC (de Heren XVII) dan diangkat menjadi gubernur Jenderal Hindia Belanda serta diangkat menjadi armada kapal");
        characters[14] = new Character("Jenderal Jan Pieterszoon coen",0,150,30,true,"Jenderal VOC keempat yang mempunyai ciri khas dari kepemimpinannya adalah politik atau adu domba atau devide et impera");
        characters[15] = new Character("Jenderal Cornelis van der Lijn",0,200,40,true,"seorang Gubernur yang berhasil memperkuat VOC");

        //Input Weapon pada character musuh
        characters[10].setWeaponDefault(weapons.get(9));//Menambahkan Luger P08 ke Jendral Pieter
        characters[11].setWeaponDefault(weapons.get(9));//Menambahkan Luger P08 ke Cornelis de Houtman
        characters[12].setWeaponDefault(weapons.get(10));//Menambahkan mp40 ke Pieter de Kaizer
        characters[13].setWeaponDefault(weapons.get(10));//Menambahkan mp40 ke Jendral Gerard Reynst
        characters[14].setWeaponDefault(weapons.get(11));//Menambahkan m1 Carbine P08 ke Jendral Jan Pieterson
        characters[15].setWeaponDefault(weapons.get(12));//Menambahkan Lee Enfield ke Jendral Cornelis

        //INPUT Armor Pada Character Musuh
        characters[11].setArmorDefault(armors.get(6)); //Menambahkan armor 1 ke Cornelis de Houtman
        characters[12].setArmorDefault(armors.get(6)); //Menambahkan armor 1 ke Pieter de Kaizer
        characters[13].setArmorDefault(armors.get(7)); //Menambahkan armor 2 ke Jendral Gerard
        characters[14].setArmorDefault(armors.get(7)); //Menambahkan armor 2 ke Jendral Pieterson
        characters[15].setArmorDefault(armors.get(8)); //Menambahkan armor 3 ke Jendral Cornelis

        for (Character c:characters){
            characterArrayList.add(c);
        }

        return characterArrayList;
    }

    static ArrayList<Weapon> inputWeapon() {
        ArrayList<Weapon> weaponList = new ArrayList<>();
        Weapon[] weapons = new Weapon[13];

        //INPUT WEAPONS
        weapons[0] = new Weapon("Bambu Runcing",300, 8, "unknown",false);
        weapons[1] = new Weapon("Celurit",450, 11, "unknown", false);
        weapons[2] = new Weapon("Klewang",500, 15, "unknown", false);
        weapons[3] = new Weapon("Kujang",600, 17, "unknown", false);
        weapons[4] = new Weapon("Golok",760, 20, "unknown", false);
        weapons[5] = new Weapon("Parang",850, 24, "unknown", false);
        weapons[6] = new Weapon("Badik",970, 29, "unknown", false);
        weapons[7] = new Weapon("Belati",2000, 32, "unknown", false);
        weapons[8] = new Weapon("Keris",4000, 40, "unknown", false);

        //Input Weapon Enemy (Belanda)
        weapons[9] = new Weapon("Luger p08",0, 28, "Pistol",true);
        weapons[10] = new Weapon("MP40",0, 40, "Submachine Gun",true);
        weapons[11] = new Weapon("M1 carbine",0, 48, "Shotgun",true);
        weapons[12] = new Weapon("Lee Enfield",0, 55, "Rifle",true);

        for (Weapon w:weapons){
            weaponList.add(w);
        }

        return weaponList;
    }

    static ArrayList<Armor> inputArmor() {
        ArrayList<Armor> armorArrayList = new ArrayList<>();
        Armor[] armors = new Armor[9];

        // INPUT ARMOR CHARACTER
        armors[0] = new Armor("Helmet",200,5,"UNKNOWN",false);
        armors[1] = new Armor("Talawang",400,10,"UNKNOWN",false);
        armors[2] = new Armor("Perisai Iman",700,17,"UNKNOWN",false);
        armors[3] = new Armor("Baju Besi",1000,25,"UNKNOWN",false);
        armors[4] = new Armor("Baju Logam",1500,31,"UNKNOWN",false);
        armors[5] = new Armor("Tameng",3000,40,"UNKNOWN",false);

        //INPUT ARMOR KEPUNYAAN ENEMY (BELANDA)
        armors[6] = new Armor("Armor 1",0,28,"UNKNOWN",true);
        armors[7] = new Armor("Armor 2",0,38,"UNKNOWN",true);
        armors[8] = new Armor("Armor 3",0,50,"UNKNOWN",true);

        for (Armor a:armors){
            armorArrayList.add(a);
        }

        return armorArrayList;
    }

    static ArrayList<Skill> inputSkill(){
        ArrayList<Skill> skillArrayList = new ArrayList<>();
        Skill[] skills = new Skill[4];

        skills[0] = new Skill("Brajamusti", 3000,30,"unknown");
        skills[1] = new Skill("Galap Ngampar", 5000,40,"unknown");
        skills[2] = new Skill("Ajian Ilmu Karang", 10000,90,"unknown");
        skills[3] = new Skill("Ajian Inti Lebur Sakheti", 13000,100,"unknown");

        for (Skill s:skills) {
            skillArrayList.add(s);
        }

        return skillArrayList;
    }

    static void about(){
        System.out.println("=====================================================================================================================");
        System.out.println("Jawara The Game merupakan game yang menceritakan tentang para karakter atau tokoh Jawara Banten yang\n" +
                "melawan atau memberontak kepada colonial Belanda pada jaman penjajahan. Terdapat beberapa karakter pada game ini dan\n" +
                "dibagi menjadi dua bagian yaitu karakter Jawara dan Kolonial Belanda.");
        System.out.println("=====================================================================================================================");
    }
}