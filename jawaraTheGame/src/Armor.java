class Armor extends Item{
    private int defencePower;
    private boolean isEnemy;

    Armor(String name,int price,int defencePower,String description,boolean isEnemy){
        super(name,price,description);
        this.defencePower = defencePower;
        this.isEnemy = isEnemy;
    }

    public int getDefencePower(){
        return this.defencePower;
    }
    public boolean getIsEnemy(){
        return this.isEnemy;
    }
}
