class Level{
    private int stage;
    private String location;
    private String description;


    Level(int stage,String location,String desc){
        this.stage = stage;
        this.location = location;
        this.description = desc;
    }

    public int getStage(){
        return this.stage;
    }
    public String getLocation(){
        return this.location;
    }

    public String getDescription() {
        return description;
    }

    void mulai(){

    }
}